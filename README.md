## Objective
Unpack torrents once they are completed
### Getting Started
#### Requirements
- python3.8
- [optional] https://hub.docker.com/r/linuxserver/transmission
#### Transmission

Update Transmission `settings.json` file.
- "script-torrent-done-enabled": true
- "script-torrent-done-filename": "/\<path to\>/unpacker.py"
