#!/usr/bin/env python3
import subprocess
import logging
import re
import sys


def init_logging() -> logging.Logger:
    """
    Create a dedicated Logger that sends messages to stdout
    >>> init_logging()
    <logging.Logger>
    """
    _logger = logging.getLogger(__file__)
    _logger.setLevel(logging.INFO)

    _log_format = logging.Formatter('%(asctime)s [%(levelname)s] [%(funcName)s]: %(message)s')
    try:
        _file_handler = logging.StreamHandler(sys.stdout)
        _file_handler.setFormatter(_log_format)
        _logger.addHandler(_file_handler)
    except PermissionError:
        sys.exit(-1)

    return _logger


def fetch_torrent_list(_host=None, _port=None) -> list:
    """
    Given an optional _host and _port, retrieve the list of torrents from Transmission
    >>> fetch_torrent_list()
    [
        '     2   100%    0 GB  Done         0.0     0.0    0.0  Idle         torrent.name.0',
        '     3   100%    0 GB  Done         0.0     0.0    0.0  Idle         torrent.name.1',
        '     4   100%    0 GB  Done         0.0     0.0    0.0  Idle         torrent.name.2'
    ]
    """
    _transmission_binary = '/usr/bin/transmission-remote'
    if _host is None or _port is None:
        _cmd = f'{_transmission_binary} --list'
    else:
        _cmd = f'{_transmission_binary} {_host}:{_port} --list'

    _output = subprocess.run(_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    _torrent_list = _output.stdout.decode('utf8').split('\n')

    # return the torrent list without the column header and the sum of torrent disk usage
    return _torrent_list[1:-2]


def get_idle_torrent_from_list(_torrent_list: list) -> list:
    """
    Iterate over a list of torrents and return those in an Idle state
    >>> get_idle_torrent_from_list(torrent_list)
    ['torrent.name.0', 'torrent.name.1', 'torrent.name.2']
    """
    _idle_torrent_name_re = re.compile('.*Idle\\s+(.*?)$')
    _idle_torrent_list = []
    for _torrent in _torrent_list:
        if _torrent_name := _idle_torrent_name_re.findall(_torrent):
            _idle_torrent_list.append(_torrent_name[0])
            LOGGER.info(f'found {_torrent_name[0]}')

    return _idle_torrent_list


def find_idle_torrent_with_extension(_tld: str, _torrent_list: list, _extension: str) -> list:
    """
    Given a _tld (top level directory), a _torrent_list and a file _extention, return a list
    of torrents matching that _extension
    >>> find_idle_torrent_with_extension('/downloads', ['torrent.name.0'], 'zip')
    ['/downloads/subfolder/torrent.name.0/torrent.name.0.zip']
    >>> find_idle_torrent_with_extension('/downloads', ['torrent.name.0'], 'rar')
    ['/downloads/subfolder/torrent.name.0/torrent.name.0.rar']
    """
    _torrents = []
    for _torrent_name in _torrent_list:
        _cmd = f"find '{_tld}' -mindepth 2 -maxdepth 3 -type d -name '{_torrent_name}'"
        _output = subprocess.run(_cmd, shell=True, stdout=subprocess.PIPE)

        _torrent_directory = _output.stdout.decode('utf8').strip()
        if (len(_torrent_directory)) > 0:
            _cmd = f"find '{_torrent_directory}' -type f -name '*.{_extension}'"
            _output = subprocess.run(_cmd, shell=True, stdout=subprocess.PIPE)
            _torrent_file = _output.stdout.decode('utf8').strip()

            if len(_torrent_file) > 0:
                LOGGER.info(f'adding {_torrent_file} for processing')
                _torrents.append(_torrent_file)

    return _torrents


def unzip_torrent(_torrent_list: list):
    """
    Given a _torrent_list where each item is a canonical path of a zip archive.
    Extract the archive in the same directory
    >>> unzip_torrent(['/downloads/subfolder/torrent.name.0/torrent.name.0.zip'])
    """
    for _torrent_zip in _torrent_list:
        _torrent_directory = get_dir_name(_torrent_zip)
        _cmd = f"unzip -n '{_torrent_zip}' -d '{_torrent_directory}'"
        LOGGER.debug(f'running {_cmd}')
        _output = subprocess.run(_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        if _output.returncode != 0:
            LOGGER.error(f'running {_cmd}')
            continue

        LOGGER.info(f'extracted {_torrent_zip}')


def get_dir_name(_path: str) -> str:
    """
    Given a canonical _path, remove the filename from it
    >>> get_dir_name('/downloads/subfolder/torrent.name.0/torrent.name.0.rar')
    '/downloads/subfolder/torrent.name.0'
    """
    return '/'.join(_path.split('/')[:-1])


def unrar_torrent(_torrent_list: list):
    """
    Given a _torrent_list where each item is a canonical path of a rar archive.
    Extract the archive in the same directory
    >>> unzip_torrent(['/downloads/subfolder/torrent.name.0/torrent.name.0.rar'])
    """
    for _torrent_rar in _torrent_list:
        _torrent_directory = get_dir_name(_torrent_rar)

        # skip already extracted files
        _cmd = f"unrar e -o- '{_torrent_rar}' '{_torrent_directory}'"
        LOGGER.debug(f'running {_cmd}')
        _output = subprocess.run(_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        if _output.returncode == 0 or _output.returncode == 10:
            LOGGER.info(f'extracted {_torrent_rar}')
            continue

        LOGGER.error(f'{_cmd}')


if __name__ == '__main__':
    LOGGER = init_logging()
    tld = '/downloads'
    extensions = ['zip', 'rar']
    torrent_list = fetch_torrent_list()
    idle_torrent_list = get_idle_torrent_from_list(torrent_list)

    for extension in extensions:
        torrents = find_idle_torrent_with_extension(tld, idle_torrent_list, extension)

        if extension == 'zip':
            unzip_torrent(torrents)
        elif extension == 'rar':
            unrar_torrent(torrents)
